import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, throwError } from 'rxjs';
import { ENV, HttpHeader, PorftolioPath } from '../enums/enum';

@Injectable({
  providedIn: 'root',
})
export class PortfolioService {
  private baseUrl = ENV.API.URL;
  private basePath = PorftolioPath.PORTFOLIOS;

  constructor(private http: HttpClient) {}

  public getAllPorfolio(): Observable<any[]> {
    return this.http
      .get<any[]>(this.getUrl())
      .pipe(catchError(this.handleError));
  }

  public deletePorfolio(id: string): Observable<any> {
    return this.http
      .delete<any>(this.getUrl() + `/${id}`)
      .pipe(catchError(this.handleError));
  }

  private handleError(err: HttpErrorResponse): Observable<never> {
    return throwError(() => err);
  }

  private getUrl(path = ''): string {
    return `${this.baseUrl}${this.basePath}${path}`;
  }
}
