import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, throwError } from 'rxjs';
import { ENV, HttpHeader, PorftolioPath, UsersPath } from '../enums/enum';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  private baseUrl = ENV.API.URL;
  private basePath = UsersPath.USERS;

  constructor(private http: HttpClient) {}

  public getAllUsers(): Observable<any[]> {
    return this.http
      .get<any[]>(this.getUrl())
      .pipe(catchError(this.handleError));
  }

  private handleError(err: HttpErrorResponse): Observable<never> {
    return throwError(() => err);
  }

  private getUrl(path = ''): string {
    return `${this.baseUrl}${this.basePath}${path}`;
  }
}
