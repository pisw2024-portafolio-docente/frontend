import {Injectable} from '@angular/core';
import {KeycloakService} from 'keycloak-angular';

@Injectable({providedIn: 'root'})
export class KeycloakOperationService {
  constructor(private readonly keycloakService: KeycloakService) { }

  isloggedIn() {
    return this.keycloakService.isLoggedIn();
  }
  logout() {
    this.keycloakService.logout();
  }
  getUserProfile() {
    return this.keycloakService.loadUserProfile();
  }

  getToken() {
    return this.keycloakService.getToken();
  }
}
