import { Injectable } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private keycloakService: KeycloakService) {}

  getRole(): string | null {
    const roles = this.keycloakService.getUserRoles();
    if (roles.includes('admin')) {
      return 'admin';
    } else if (roles.includes('teacher')) {
      return 'teacher';
    }
    return null;
  }
}


