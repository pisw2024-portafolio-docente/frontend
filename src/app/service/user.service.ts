import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private readonly REMEMBERED_USERNAME_KEY = 'rememberedUsername';

  constructor(private cookieService: CookieService) { }

  saveRememberedUsername(username: string) {
    this.cookieService.set(this.REMEMBERED_USERNAME_KEY, username);
  }

  getRememberedUsername(): string | null {
    return this.cookieService.get(this.REMEMBERED_USERNAME_KEY);
  }

  removeRememberedUsername() {
    this.cookieService.delete(this.REMEMBERED_USERNAME_KEY);
  }
  saveToken(token: string) {
    this.cookieService.set('token', token);
  }

  saveUsername(username: string) {
    this.cookieService.set('username', username);
  }

  getUsername() {
    return this.cookieService.get('username');
  }

  getToken() {
    return this.cookieService.get('token');
  }

  removeToken() {
    this.cookieService.delete('token');
  }
}
