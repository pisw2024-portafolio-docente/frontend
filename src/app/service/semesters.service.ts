import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, throwError } from 'rxjs';
import { ENV, HttpHeader, PorftolioPath } from '../enums/enum';
import { SemestrePath } from '../enums/api/semestre-path.enum';
import { Semester } from '../models/semester.model';

@Injectable({
  providedIn: 'root',
})
export class SemesterService {
  private baseUrl = ENV.API.URL;
  private basePath = SemestrePath.SEMESTRES;

  constructor(private http: HttpClient) {}

  public getAllSemestres(): Observable<Semester[]> {
    return this.http
      .get<Semester[]>(this.getUrl())
      .pipe(catchError(this.handleError));
  }

  private handleError(err: HttpErrorResponse): Observable<never> {
    return throwError(() => err);
  }

  private getUrl(path = ''): string {
    return `${this.baseUrl}${this.basePath}${path}`;
  }
}
