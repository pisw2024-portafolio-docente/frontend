import { KeycloakService } from "keycloak-angular";
import { environment } from "../../enviroment/enviroment";

export function initializeKeycloak(keycloak: KeycloakService) {
  return () =>
    keycloak.init({
      config: {
        url: environment.keycloak.url,
        realm: environment.keycloak.realm,
        clientId: environment.keycloak.clientId,
      },

      initOptions: {
        onLoad: 'check-sso',
        flow: 'implicit',
        silentCheckSsoRedirectUri: window.location.origin + '/assets/silent-check-sso.html',
      },
    });
}
