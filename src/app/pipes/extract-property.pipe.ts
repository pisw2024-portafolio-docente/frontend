import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'extractProperty',
  standalone: true
})
export class ExtractPropertyPipe implements PipeTransform {

  transform(items: any[], propertyName: string): any[] {
    if (!items) {
      return [];
    }

    return items.map(item => item[propertyName]);
  }

}
