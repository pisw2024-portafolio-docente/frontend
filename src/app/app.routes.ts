import { Routes } from '@angular/router';
import { LoginComponent } from './views/pages/login/login.component';
import { HomeComponent } from './views/pages/home/home.component';
import { AuthGuard } from './guard/auth.guard';

export const routes: Routes = [
  {
    path: '',
    component: LoginComponent,
  },
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'admin',
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./views/pages/admin/admin-routes').then((mod) => mod.ADMIN_ROUTES),
    data: {
      roles: ['admin']
    }
  },
  {
    path:'teacher',
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./views/pages/teacher/teacher-routes').then((mod) => mod.TEACHER_ROUTES),
    data: {
      roles: ['teacher']
    }
  }
];
