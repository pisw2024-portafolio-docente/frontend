export const REGISTERS =[
  {
    "id": "1001",
    "nombre": "Fase 1",
    "fechaInicio": "2024-06-01",
    "fechaFin": "2024-06-05",
    "status": true,
  },
  {
    "id": "1002",
    "nombre": "Fase 2",
    "fechaInicio": "2024-07-10",
    "fechaFin": "2024-07-15",
    "status": false,
  },
  {
    "id": "1003",
    "nombre": "Fase 3",
    "fechaInicio": "2024-08-20",
    "fechaFin": "2024-08-25",
    "status": true,
  },
]
