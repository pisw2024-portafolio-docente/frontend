import { Portfolio } from '../models/Portfolio.model';
import { UserPortfolio } from '../models/User-portfolio.model';

export const REGISTERS = [
  {
    id: '1001',
    nombre: 'Evento A',
    fechaInicio: '2024-06-01',
    fechaFin: '2024-06-05',
    status: true,
    year: 2024,
  },
  {
    id: '1002',
    nombre: 'Evento B',
    fechaInicio: '2024-07-10',
    fechaFin: '2024-07-15',
    status: false,
    year: 2024,
  },
  {
    id: '1003',
    nombre: 'Evento C',
    fechaInicio: '2024-08-20',
    fechaFin: '2024-08-25',
    status: true,
    year: 2024,
  },
  {
    id: '1004',
    nombre: 'Evento D',
    fechaInicio: '2024-09-05',
    fechaFin: '2024-09-10',
    status: false,
    year: 2024,
  },
  {
    id: '1005',
    nombre: 'Evento E',
    fechaInicio: '2024-10-15',
    fechaFin: '2024-10-20',
    status: true,
    year: 2024,
  },
];

export const PORTFOLIOS: Portfolio[] = [
  {
    id: '1',
    nombre: 'Proyecto A',
    responsable: 'Juan Pérez',
    year: '2023',
    semester: 'Primero',
    status: 'Activo',
  },
  {
    id: '2',
    nombre: 'Proyecto B',
    responsable: 'María López',
    year: '2023',
    semester: 'Segundo',
    status: 'Inactivo',
  },
  {
    id: '3',
    nombre: 'Proyecto C',
    responsable: 'Carlos Ruiz',
    year: '2024',
    semester: 'Primero',
    status: 'Activo',
  },
];

export const USERPORTFOLIOS: UserPortfolio[] = [
  {
    nombres: 'Juan',
    apellidos: 'Pérez',
    correo: 'juan.perez@example.com',
    rol: 'Administrador',
    status: 'Activo',
  },
  {
    nombres: 'María',
    apellidos: 'García',
    correo: 'maria.garcia@example.com',
    rol: 'Usuario',
    status: 'Inactivo',
  },
  {
    nombres: 'Luis',
    apellidos: 'Martínez',
    correo: 'luis.martinez@example.com',
    rol: 'Moderador',
    status: 'Activo',
  },
  {
    nombres: 'Ana',
    apellidos: 'López',
    correo: 'ana.lopez@example.com',
    rol: 'Usuario',
    status: 'Activo',
  },
  {
    nombres: 'Carlos',
    apellidos: 'Fernández',
    correo: 'carlos.fernandez@example.com',
    rol: 'Administrador',
    status: 'Inactivo',
  },
  {
    nombres: 'Sofía',
    apellidos: 'Ramírez',
    correo: 'sofia.ramirez@example.com',
    rol: 'Usuario',
    status: 'Activo',
  },
];
