import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';

import { Table, TableModule } from 'primeng/table';
import { CommonModule } from '@angular/common';
import { FileUploadModule } from 'primeng/fileupload';

import { RadioButtonModule } from 'primeng/radiobutton';
import { FormsModule } from '@angular/forms';
import { REGISTERS } from '../../../../utils/dummyData';

export interface Register {
  id: string;
  nombre?: string;
  fechaInicio?: string;
  fechaFin?: string;
  status?: boolean;
}

@Component({
  selector: 'app-table-phase',
  standalone: true,
  imports: [
    TableModule,
    CommonModule,
    FileUploadModule,
    RadioButtonModule,
    FormsModule,
  ],

  templateUrl: './table-phase.component.html',
  styleUrl: './table-phase.component.css',
})
export class TablePhaseComponent implements OnInit {
  @ViewChild('dt') dtRef!: Table;

  @Output() canDelete = new EventEmitter<boolean>();
  @Output() deleteSource = new EventEmitter<String[]>();

  @Input() source!: Register[];

  @Input() set removeDelete(value: boolean) {
    if (!value) return;
    const id_removes: String[] = this.selectedData
      ? this.selectedData.map((el) => el.id)
      : [];

    this.deleteSource.emit(id_removes);
    this.selectedData = null;
  }
  @Input() set search(val: string) {
    this.dtRef?.filterGlobal(val, 'contains');
  }

  _selectedData!: Register[] | null;

  set selectedData(data: Register[] | null) {
    this._selectedData = data;
    this.canDelete.emit(
      this._selectedData ? this._selectedData.length !== 0 : false
    );
  }
  get selectedData(): Register[] | null {
    return this._selectedData;
  }

  handleFliterGlobal(event: Event, dt1: Table) {
    const target = event.target as HTMLInputElement;
    if (target) {
      dt1.filterGlobal(target.value, 'contains');
    }
  }
  ngOnInit() {}
}
