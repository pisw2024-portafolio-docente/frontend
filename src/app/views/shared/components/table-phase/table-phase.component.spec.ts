import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TablePhaseComponent } from './table-phase.component';

describe('TablePhaseComponent', () => {
  let component: TablePhaseComponent;
  let fixture: ComponentFixture<TablePhaseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TablePhaseComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(TablePhaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
