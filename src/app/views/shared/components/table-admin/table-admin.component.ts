import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';

import { Table, TableModule } from 'primeng/table';
import { CommonModule } from '@angular/common';
import { FileUploadModule } from 'primeng/fileupload';

import { RadioButtonModule } from 'primeng/radiobutton';
import { FormsModule } from '@angular/forms';
import { ExtractPropertyPipe } from '../../../../pipes/extract-property.pipe';
import { Header } from '../../../../models/Header.model';
import { Router } from '@angular/router';

export interface Register {
  id: string;
  nombre?: string;
  year?: number;
  fechaInicio?: string;
  fechaFin?: string;
  status?: boolean;
}

@Component({
  selector: 'app-table-admin',
  standalone: true,
  templateUrl: './table-admin.component.html',
  styleUrl: './table-admin.component.css',
  imports: [
    TableModule,
    CommonModule,
    FileUploadModule,
    RadioButtonModule,
    FormsModule,
    ExtractPropertyPipe,
  ],
})
export class TableAdminComponent {
  constructor(private router: Router) {}

  @ViewChild('dt') dtRef!: Table;

  @Output() canDelete = new EventEmitter<boolean>();
  @Output() deleteSource = new EventEmitter<String[]>();

  @Input() source!: any[];

  @Input() headers!: Header[];

  @Input() redirectUrl: string = '';

  @Input() set removeDelete(value: boolean) {
    if (!value) return;
    const id_removes: String[] = this.selectedData
      ? this.selectedData.map((el) => el.id)
      : [];

    this.deleteSource.emit(id_removes);
    this.selectedData = null;
  }
  @Input() set search(val: string) {
    this.dtRef?.filterGlobal(val, 'contains');
  }

  _selectedData!: any[] | null;

  set selectedData(data: any[] | null) {
    this._selectedData = data;
    this.canDelete.emit(
      this._selectedData ? this._selectedData.length !== 0 : false
    );
  }
  get selectedData(): any[] | null {
    return this._selectedData;
  }

  handleFliterGlobal(event: Event, dt1: Table) {
    const target = event.target as HTMLInputElement;
    if (target) {
      dt1.filterGlobal(target.value, 'contains');
    }
  }
  redirect(id: string): void {
    const route = this.redirectUrl + id;
    this.router.navigate([route]);
  }
}
