import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { InputSwitchModule } from 'primeng/inputswitch';
import { RoutesAdmin } from '../../../../enums/app/routesAdmin.enum';
import { SidebarLink } from '../../../../types/sidebarLink.type';
import { AuthService } from '../../../../service/auth.service';
import { RoutesTeacher } from '../../../../enums/app/routesTeacher.enum';
import { KeycloakOperationService } from '../../../../service/keycloak.service';

@Component({
  selector: 'app-sidebar',
  standalone: true,
  imports: [CommonModule, InputSwitchModule, FormsModule],
  templateUrl: './sidebar.component.html',
  styleUrl: './sidebar.component.css',
})
export class SidebarComponent implements OnInit {

  menuItems: SidebarLink[] = [];
  isDarkMode = false;
  private userRole: string | null = null;


  constructor(
    private router: Router,
    private authService: AuthService,
    private keycloakService: KeycloakOperationService
  ) {}

  ngOnInit(): void {
    this.userRole = this.authService.getRole();
    this.setMenuItems();
  }

  setMenuItems(): void {
    if (this.userRole === 'admin') {
      this.menuItems = [
        {
          icon: 'school',
          name: 'Semestres académicos',
          isActive: true,
          base: RoutesAdmin.BASE,
          route: RoutesAdmin.HOME,
        },
        {
          icon: 'person',
          name: 'Usuarios',
          isActive: false,
          base: RoutesAdmin.BASE,
          route: RoutesAdmin.USERS,
        },
        {
          icon: 'folder',
          name: 'Portafolios',
          isActive: false,
          base: RoutesAdmin.BASE,
          route: RoutesAdmin.PORTFOLIO,
        },
        { icon: 'help', name: 'Ayuda y Soporte', isActive: false },
        { icon: 'person_outline', name: 'Perfil', isActive: false },
        { icon: 'settings', name: 'Configuración', isActive: false },
        { icon: 'logout', name: 'Cerrar sesión', isActive: false, onClick: () => this.logout() },
      ];
    } else if (this.userRole === 'teacher') {
      this.menuItems = [
        {
          icon: 'home',
          name: 'Inicio',
          isActive: true,
          route: RoutesTeacher.HOME,
        },
        { icon: 'help', name: 'Ayuda y Soporte', isActive: false },
        { icon: 'person_outline', name: 'Perfil', isActive: false },
        { icon: 'settings', name: 'Configuración', isActive: false },
        { icon: 'logout', name: 'Cerrar sesión', isActive: false, onClick: () => this.logout() },
      ];
    }
  }

  handleClickMenuItem(index: number): void {
    this.menuItems.forEach((el, i) => {
      el.isActive = i === index;
    });

    const sidebarLink = this.menuItems[index];
    if (sidebarLink.onClick) {
      sidebarLink.onClick();
    }
    const route = (sidebarLink.base ?? '') + (sidebarLink.route ?? '');
    this.router.navigate([route]);
  }

  handleClickTheme() {
    this.isDarkMode = !this.isDarkMode;
  }

  logout() {
    this.keycloakService.logout();
  }
}
