import { CommonModule } from '@angular/common';
import { Component, OnDestroy } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AuthService } from '../../../service/auth.service';
import { Router } from '@angular/router';

import { Subject, takeUntil } from 'rxjs';
import { KeycloakService } from 'keycloak-angular';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [ReactiveFormsModule, CommonModule, HttpClientModule],
  templateUrl: './login.component.html',
  styleUrl: './login.component.css',
  providers: [AuthService],
})
export class LoginComponent {

  constructor(
    private keycloak: KeycloakService,
  ) {
  }

  async doLogin(): Promise<void> {
      await this.keycloak.login( {
        redirectUri: window.location.origin + '/home',
      })
  }
}
