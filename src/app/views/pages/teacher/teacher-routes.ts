import { Route } from "@angular/router";
import { RoutesTeacher } from "../../../enums/app/routesTeacher.enum";
import { HomeTeacherComponent } from "./home-teacher/home-teacher.component";

export const TEACHER_ROUTES: Route[] = [
  {
    path: RoutesTeacher.HOME,
    component: HomeTeacherComponent,
  }
]
