import { Component } from '@angular/core';
import { NavbarComponent } from '../../../shared/components/navbar/navbar.component';
import { SidebarComponent } from '../../../shared/components/sidebar/sidebar.component';

@Component({
  selector: 'app-home-teacher',
  standalone: true,
  imports: [
    NavbarComponent,
    SidebarComponent
  ],
  templateUrl: './home-teacher.component.html',
  styleUrl: './home-teacher.component.css'
})
export class HomeTeacherComponent {

  menuItems = [
    { icon: 'school', name: 'Mis Cursos', isActive: false },
    { icon: 'help', name: 'Ayuda y Soporte', isActive: false },
    { icon: 'person_outline', name: 'Perfil', isActive: true },
    { icon: 'settings', name: 'Configuración', isActive: false },
    { icon: 'logout', name: 'Cerrar sesión', isActive: false }
  ];
  isDarkMode = false;
  handleClickMenuItem(index: number){
    this.menuItems.forEach((el,i)=>{
      el.isActive = i === index;
    })
  }
  handleClickTheme(){
    this.isDarkMode = !this.isDarkMode;
  }
}
