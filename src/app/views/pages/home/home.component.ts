import { Component, OnInit } from '@angular/core';
import { SidebarComponent } from '../../shared/components/sidebar/sidebar.component';
import { NavbarComponent } from '../../shared/components/navbar/navbar.component';
import { KeycloakOperationService } from '../../../service/keycloak.service';
@Component({
  selector: 'app-home',
  standalone: true,
  imports: [NavbarComponent, SidebarComponent],
  templateUrl: './home.component.html',
  styleUrl: './home.component.css',
  providers: [KeycloakOperationService],
})
export class HomeComponent implements OnInit {

  userProfile: any|null = null;

  constructor(
    private keycloakService: KeycloakOperationService
  ) {}

  ngOnInit(): void {
    console.log("Home Component Loaded");
    this.keycloakService.getUserProfile().then((data: any) => {
      this.userProfile = data;
      console.log(this.userProfile);
    })
  }

  logout() {
    this.keycloakService.logout();
  }
}
