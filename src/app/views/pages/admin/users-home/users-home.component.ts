import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { TableAdminComponent } from '../../../shared/components/table-admin/table-admin.component';
import { USERPORTFOLIOS } from '../../../../utils/dummyData';
import { UserPortfolio } from '../../../../models/User-portfolio.model';
import { Header } from '../../../../models/Header.model';

@Component({
  selector: 'app-users-home',
  standalone: true,
  imports: [FormsModule, TableAdminComponent],
  templateUrl: './users-home.component.html',
  styleUrl: './users-home.component.css'
})
export class UsersHomeComponent {
  search = ""
  source: UserPortfolio[] = USERPORTFOLIOS;
  headers: Header[] = [
    {
      key: 'id',
      value: 'id',
      styles: '',
    },
    {
      key: 'nombres',
      value: 'Nombres',
      styles: '',
    },
    {
      key: 'apellidos',
      value: 'Apellidos',
      styles: '',
    },
    {
      key: 'correo',
      value: 'Correo',
      styles: '',
    },
    {
      key: 'rol',
      value: 'Rol',
      styles: '',
    },
    {
      key: 'status',
      value: 'Estado',
      styles: '',
    },
  ];
}
