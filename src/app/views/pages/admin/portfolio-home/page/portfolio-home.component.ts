import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Header } from '../../../../../models/Header.model';
import { TableAdminComponent } from '../../../../shared/components/table-admin/table-admin.component';
import { PORTFOLIOS } from '../../../../../utils/dummyData';
import { Portfolio } from '../../../../../models/Portfolio.model';
import { CommonModule } from '@angular/common';
import { DialogModule } from 'primeng/dialog';
import { RoutesAdmin } from '../../../../../enums/app/routesAdmin.enum';
import { StudyPlanComponent } from '../components/study-plan/study-plan.component';
import { ManualLoadComponent } from '../components/manual-load/manual-load.component';
import { AutomaticPortfolioLoadComponent } from '../components/automatic-portfolio-load/automatic-portfolio-load.component';
import { Subject, takeUntil } from 'rxjs';
import { PortfolioService } from '../../../../../service/portfolio.service';

@Component({
  selector: 'app-portfolio-home',
  standalone: true,
  imports: [
    FormsModule,
    TableAdminComponent,
    CommonModule,
    DialogModule,
    StudyPlanComponent,
    ManualLoadComponent,
    AutomaticPortfolioLoadComponent,
  ],
  templateUrl: './portfolio-home.component.html',
  styleUrl: './portfolio-home.component.css',
})
export class PortfolioHomeComponent implements OnInit, OnDestroy {
  constructor(private portfolioService: PortfolioService) {}
  redirectUrl = RoutesAdmin.BASE + RoutesAdmin.DETAILSPORTFOLIO;
  search = '';
  source: Portfolio[] = [];
  headers: Header[] = [
    {
      key: 'id',
      value: 'id',
      styles: '',
    },
    {
      key: 'nombre',
      value: 'Nombre',
      styles: '',
    },
    {
      key: 'responsable',
      value: 'Responsable',
      styles: '',
    },
    {
      key: 'year',
      value: 'Año',
      styles: '',
    },
    {
      key: 'semester',
      value: 'Semestre',
      styles: '',
    },
    {
      key: 'status',
      value: 'Estado',
      styles: '',
    },
  ];
  visible: boolean = false;
  visibleStudyPlan: boolean = false;
  visibleManualLoad: boolean = false;
  visibleAutomaticLoad: boolean = false;
  disableDeleteButton = false;
  canDelete = false;
  private destroy$ = new Subject<void>();

  ngOnInit(): void {
    this.portfolioService
      .getAllPorfolio()
      .pipe(takeUntil(this.destroy$))
      .subscribe((portfolios) => (this.source = portfolios));
  }
  showDialog() {
    this.visible = true;
  }

  confirmDeleteDialog() {
    this.visible = false;
    this.canDelete = true;
    this.disableDeleteButton = false;
  }

  deleteSourcePrepare(ids: String[]) {
    setTimeout(() => {
      this.canDelete = false;

      this.source = this.source.filter((val) => !ids.includes(val.id));
    });
  }

  handleCloseModalStudyPlan(visible: boolean) {
    this.visibleStudyPlan = visible;
  }
  handleCloseModalManualLoad(visible: boolean) {
    this.visibleManualLoad = visible;
  }
  handleCloseModalAutomaticLoad(visible: boolean) {
    this.visibleAutomaticLoad = visible;
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
