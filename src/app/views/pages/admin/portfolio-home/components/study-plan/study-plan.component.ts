import { CommonModule } from '@angular/common';
import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { DialogModule } from 'primeng/dialog';
import { FileUploadModule, UploadEvent } from 'primeng/fileupload';
import { FileUploaderComponent } from '../file-uploader/file-uploader.component';

@Component({
  selector: 'app-study-plan',
  standalone: true,
  imports: [DialogModule, FileUploadModule, CommonModule, FileUploaderComponent],
  templateUrl: './study-plan.component.html',
  styleUrl: './study-plan.component.css'
})
export class StudyPlanComponent {
  @Input()
  visible: boolean = false;

  @Output()
  onCloseModal = new EventEmitter<boolean>();
  
  @ViewChild('fileInput') fileInput!: ElementRef;
  fileName: string | null = null;
  uploadedFiles: any[] = [];

  handleCloseModal() {
    this.onCloseModal.emit(false);
  }
  onUpload(event:UploadEvent){}

  onFileSelected(event: any): void {
    const file = event.target.files[0];
    this.fileName = file.name;
  }

  removeFile(): void {
    this.fileName = null;
    this.fileInput.nativeElement.value = '';
  }

}
