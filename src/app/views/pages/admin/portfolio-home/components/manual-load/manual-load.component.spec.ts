import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManualLoadComponent } from './manual-load.component';

describe('ManualLoadComponent', () => {
  let component: ManualLoadComponent;
  let fixture: ComponentFixture<ManualLoadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ManualLoadComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ManualLoadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
