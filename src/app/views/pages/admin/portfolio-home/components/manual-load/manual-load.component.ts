import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DialogModule } from 'primeng/dialog';

@Component({
  selector: 'app-manual-load',
  standalone: true,
  imports: [DialogModule, FormsModule, CommonModule],
  templateUrl: './manual-load.component.html',
  styleUrl: './manual-load.component.css'
})
export class ManualLoadComponent {
  @Input()
  visible: boolean = false;

  @Output()
  onCloseModal = new EventEmitter<boolean>();
 
  options = [
    {id: 1, name: 'Option 1'},
    {id: 2, name: 'Option 2'},
    {id: 3, name: 'Option 3'}
  ];
  selectedOption: any[] = [];
 
 

  handleCloseModal() {
    this.onCloseModal.emit(false);
  }
}
