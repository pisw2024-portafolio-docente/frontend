import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DialogModule } from 'primeng/dialog';

@Component({
  selector: 'app-automatic-portfolio-load',
  standalone: true,
  imports: [DialogModule, FormsModule, CommonModule],
  templateUrl: './automatic-portfolio-load.component.html',
  styleUrl: './automatic-portfolio-load.component.css'
})
export class AutomaticPortfolioLoadComponent {
  @Input()
  visible: boolean = false;

  @Output()
  onCloseModal = new EventEmitter<boolean>();
 
  options = [
    {id: 1, name: 'Option 1'},
    {id: 2, name: 'Option 2'},
    {id: 3, name: 'Option 3'}
  ];
  selectedOption: any[] = [];
 
 

  handleCloseModal() {
    this.onCloseModal.emit(false);
  }
}
