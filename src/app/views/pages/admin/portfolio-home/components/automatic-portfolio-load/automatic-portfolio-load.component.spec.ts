import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AutomaticPortfolioLoadComponent } from './automatic-portfolio-load.component';

describe('AutomaticPortfolioLoadComponent', () => {
  let component: AutomaticPortfolioLoadComponent;
  let fixture: ComponentFixture<AutomaticPortfolioLoadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AutomaticPortfolioLoadComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(AutomaticPortfolioLoadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
