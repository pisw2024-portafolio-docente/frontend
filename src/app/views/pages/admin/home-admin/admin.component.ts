import { Component, OnInit } from '@angular/core';
import {
  Register,
  TableAdminComponent,
} from '../../../shared/components/table-admin/table-admin.component';
import { DialogModule } from 'primeng/dialog';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';

import { CommonModule } from '@angular/common';
import { REGISTERS } from '../../../../utils/dummyData';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Header } from '../../../../models/Header.model';
import { RoutesAdmin } from '../../../../enums/app/routesAdmin.enum';
import { SemesterService } from '../../../../service/semesters.service';
import { Subject, takeUntil } from 'rxjs';
import { Semester } from '../../../../models/semester.model';

@Component({
  selector: 'app-admin',
  standalone: true,
  imports: [
    TableAdminComponent,
    DialogModule,
    ButtonModule,
    InputTextModule,
    CommonModule,
    FormsModule,
  ],
  templateUrl: './admin.component.html',
  styleUrl: './admin.component.css',
})
export class AdminComponent implements OnInit {
  source: Semester[] = [];
  headers: Header[] = [
    {
      key: 'id',
      value: 'id',
      styles: '',
    },
    {
      key: 'name',
      value: 'Nombre',
      styles: '',
    },
    {
      key: 'year',
      value: 'Año',
      styles: '',
    },
    {
      key: 'startAt',
      value: 'Fecha de Inicio',
      styles: '',
    },
    {
      key: 'endsAt',
      value: 'Fecha de Inicio',
      styles: '',
    },
    {
      key: 'status',
      value: 'Estado',
      styles: '',
    },
  ];
  visible: boolean = false;
  disableDeleteButton = false;
  canDelete = false;
  search: string = '';
  private destroy$ = new Subject<void>();

  constructor(private router: Router, private route: ActivatedRoute,  private semesterService: SemesterService) {}
  ngOnInit(): void {
    this.route.queryParams.subscribe((params) => {
      const newSemester: Register = {
        id: params['id'] || '',
        nombre: params['nombre'] || '',
        fechaInicio: params['fechaInicio'] || '',
        fechaFin: params['fechaFin'] || '',
        status: params['status'] || false,
        year: params['year'] || '',
      };

      if (newSemester.id) {
        this.addNewSemesterToList(newSemester);
      }
    });

    this.semesterService
      .getAllSemestres()
      .pipe(takeUntil(this.destroy$))
      .subscribe((semesters) => (this.source = semesters));
  }

  addNewSemesterToList(newSemester: any) {
    this.source.push(newSemester);
  }

  showDialog() {
    this.visible = true;
  }

  confirmDeleteDialog() {
    this.visible = false;
    this.canDelete = true;
    this.disableDeleteButton = false;
  }

  deleteSourcePrepare(ids: String[]) {
    setTimeout(() => {
      this.canDelete = false;

      this.source = this.source.filter((val) => !ids.includes(val.id+""));
    });
  }

  addNewSemester() {
    this.router.navigate(["/admin/"+RoutesAdmin.ADDSEMESTER]);
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
