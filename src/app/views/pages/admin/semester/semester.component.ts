import { Component } from '@angular/core';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { Register, TableAdminComponent } from '../../../shared/components/table-admin/table-admin.component';

import { CommonModule } from '@angular/common';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { InputTextModule } from 'primeng/inputtext';
import { CalendarModule } from 'primeng/calendar';
import { CascadeSelectModule } from 'primeng/cascadeselect';
import { TablePhaseComponent } from '../../../shared/components/table-phase/table-phase.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-semester',
  standalone: true,
  imports: [
    TableAdminComponent,
    DialogModule,
    ButtonModule,
    InputTextModule,
    CommonModule,
    CalendarModule,
    CascadeSelectModule,
    FormsModule,
    ReactiveFormsModule,
    TablePhaseComponent
  ],
  templateUrl: './semester.component.html',
  styleUrl: './semester.component.css'
})
export class SemesterComponent {
  semesterForm: FormGroup;
  phaseForm: FormGroup;
  source: Register[] = [];

  visibleAddPhase: boolean = false;
  visibleDeleteConfirmation: boolean = false;
  disableDeleteButton = false;
  canDelete = false;
  search: string = "";

  constructor(
    private router: Router,
    private fb: FormBuilder
  ) {
    this.semesterForm = this.fb.group({
      name: ['', Validators.required],
      year: [null, Validators.required],
      semester: ['', Validators.required],
      startDate: [null, Validators.required],
      endDate: [null, Validators.required]
    });

    this.phaseForm = this.fb.group({
      name: ['', Validators.required],
      startDate: [null, Validators.required],
      endDate: [null, Validators.required]
    });
  }

  ngOnInit() {
  }

  showDialog() {
    this.visibleDeleteConfirmation = true;
  }

  confirmDeleteDialog() {
    this.visibleDeleteConfirmation = false;
    this.canDelete = true;
  }

  deleteSourcePrepare(ids: String[]) {
    this.source = this.source.filter((val) => !ids.includes(val.id));
    this.canDelete = false;
  }

  onSubmitSemester() {
    if (this.semesterForm.valid) {
      const id = this.generateId();
      const nombre = this.semesterForm.get('name')!.value;
      const fechaInicio = this.formatDate(this.semesterForm.get('startDate')!.value);
      const fechaFin = this.formatDate(this.semesterForm.get('endDate')!.value);
      const status = true;
      const year = new Date().getFullYear();

      const eventData = {
        id,
        nombre,
        fechaInicio,
        fechaFin,
        status,
        year
      };

      this.router.navigate(['/admin'], { queryParams: eventData });
    }
  }

  onSubmitPhase() {
    if (this.phaseForm.valid) {
      const newPhase: Register = {
        id: this.generateId(),
        nombre: this.phaseForm.get('name')!.value,
        fechaInicio: this.formatDate(this.phaseForm.get('startDate')!.value),
        fechaFin: this.formatDate(this.phaseForm.get('endDate')!.value),
        status: true,
        year: new Date().getFullYear()
      };
      this.source.push(newPhase);
      console.log('Saved Phase:', newPhase);
      this.phaseForm.reset();
      this.visibleAddPhase = false;
    }
  }

  onAddPhase() {
    this.visibleAddPhase = true;
  }

  savePhase() {
    this.phaseForm.reset();
    this.visibleAddPhase = false;
  }

  private formatDate(date: Date): string {
    if (!date) return '';
    const year = date.getFullYear();
    const month = ('0' + (date.getMonth() + 1)).slice(-2);
    const day = ('0' + date.getDate()).slice(-2);
    return `${year}-${month}-${day}`;
  }

  private generateId(): string {
    return '_' + Math.random().toString(36).substr(2, 9);
  }

}
