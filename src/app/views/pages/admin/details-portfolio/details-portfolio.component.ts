import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';

@Component({
  selector: 'app-details-portfolio',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './details-portfolio.component.html',
  styleUrl: './details-portfolio.component.css'
})
export class DetailsPortfolioComponent {
  menuItems = [
    {
      name: 'Docentes',
      isActive: true,
    },
    {
      name: 'Sílabo',
      isActive: false,
    },
    {
      name: 'Examen De Entrada',
      isActive: false,
    },
    {
      name: 'Fase 1',
      isActive: false,
    },
    {
      name: 'Fase 2',
      isActive: false,
    },
    {
      name: 'Fase 3',
      isActive: false,
    },
  ];

  handleClickMenuItem(index: number) {
    this.menuItems.forEach((el, i) => {
      el.isActive = i === index;
    });
  }
  
}
