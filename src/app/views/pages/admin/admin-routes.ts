import { Route } from '@angular/router';
import { AdminComponent } from './home-admin/admin.component';
import { SemesterComponent } from './semester/semester.component';
import { PortfolioHomeComponent } from './portfolio-home/page/portfolio-home.component';
import { UsersHomeComponent } from './users-home/users-home.component';
import { RoutesAdmin } from '../../../enums/app/routesAdmin.enum';
import { DetailsPortfolioComponent } from './details-portfolio/details-portfolio.component';

export const ADMIN_ROUTES: Route[] = [
  {
    path: RoutesAdmin.HOME,
    component: AdminComponent,
  },
  {
    path: RoutesAdmin.ADDSEMESTER,
    component: SemesterComponent,
  },
  {
    path: RoutesAdmin.PORTFOLIO,
    component: PortfolioHomeComponent,
  },
  {
    path: RoutesAdmin.DETAILSPORTFOLIO + ':id',
    component: DetailsPortfolioComponent,
  },
  {
    path: RoutesAdmin.USERS,
    component: UsersHomeComponent,
  },
];
