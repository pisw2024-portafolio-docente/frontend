const  VITE_API_URL  = "https://portafolio-docente-backend-dev.onrender.com/api/v1";

const ENV = {
  API: {
    URL: VITE_API_URL,
  },
} as const;

export { ENV };
