const RoutesAdmin = {
  BASE: 'admin/',
  HOME: '',
  PORTFOLIO: 'portfolio',
  ADDSEMESTER: 'semester/add',
  DETAILSPORTFOLIO: 'portfolio/details/',
  USERS: 'users',
} as const;

export { RoutesAdmin };
