const HttpHeader = {
  CONTENT_TYPE: 'content-type',
  AUTHORIZATION: 'Authorization',
} as const;

export { HttpHeader };
