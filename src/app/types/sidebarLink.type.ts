import { type RoutesAdmin } from '../enums/enum';
import { type ValueOf } from './value-of.type';

type SidebarLink = {
  icon: string;
  name: string;
  base?: string;
  isActive: boolean;
  route?: ValueOf<typeof RoutesAdmin>;
  onClick?: () => void;
};

export { type SidebarLink };
