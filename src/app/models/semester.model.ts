type Semester = {
  id: number;
  name: string;
  year: number;
  startAt: string;
  endsAt: string;
  createdAt: string;
};
export { Semester };
