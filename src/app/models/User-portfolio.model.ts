export interface UserPortfolio {
  nombres: string;
  apellidos: string;
  correo: string;
  rol: string;
  status: string;
}