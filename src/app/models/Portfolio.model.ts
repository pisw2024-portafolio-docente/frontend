export interface Portfolio {
  id: string;
  nombre: string;
  responsable: string;
  year: string;
  semester: string;
  status: string;
}