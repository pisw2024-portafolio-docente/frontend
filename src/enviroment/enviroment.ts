export const environment = {
  API_URL: 'https://portafolio-docente-backend-dev.onrender.com',
  production: false,
  keycloak: {
    url: 'https://portafolio-docente-sso.fly.dev',
    realm: 'portafolio-docente-dev',
    clientId: 'backend',
  }
};
