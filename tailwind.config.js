/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{html,ts}",
  ],
  theme: {
    extend: {
      colors: {
        customRed: '#6A2733',
      },
    },
  },
  colors: {
    custom: '#6A2733',
    secondary: '#CAB0B5',
  },
  plugins: [],
}
